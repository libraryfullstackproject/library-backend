import 'reflect-metadata'
import express from 'express'
import routes from './routes'
import errorHandler from './middleware/error-handler'
import { jsonHeaders } from './middleware/add-headers'

const app = express()

//Express initialization
app.use(express.json())

//Middleware
app.use(jsonHeaders)

//Routes
app.use('/', routes)

//Error Handler
app.use(errorHandler)

export default app
