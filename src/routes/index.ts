import { Router, Request, Response } from 'express'
import books from './books'
import users from './users'

const router: Router = Router()

router.use('/books', books)
router.use('/users', users)

router.get('/', (req: Request, res: Response) => {
  return res.status(200).json({ message: 'Library Backend API ' })
})

export default router
