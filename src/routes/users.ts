import { Router } from 'express'
import { autoCatch } from '../middleware/auto-catch'
import UserController from '../controller/user-controller'
import { requireCredentials } from '../authentication/validate-auth'

const router: Router = Router()
const userController: UserController = new UserController()

router.post('/login', requireCredentials, autoCatch(userController.login))
router.post('/register', autoCatch(userController.register))

export default router
