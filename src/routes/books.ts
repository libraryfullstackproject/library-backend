import { Router } from 'express'
import { autoCatch } from '../middleware/auto-catch'
import BookController from '../controller/book-controller'
import { validateRequest } from '../middleware/add-headers'
import { requireAdminRole } from '../authentication/authorization/validate-privileges'

const router: Router = Router()
const bookController: BookController = new BookController()

router.get('/', autoCatch(bookController.findAllBooks))
router.get('/:title', autoCatch(bookController.findBookByTitle))
router.post('/', validateRequest, requireAdminRole, autoCatch(bookController.addBook))

export default router
