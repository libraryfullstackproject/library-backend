import BookService from '../service/book-service'
import { NextFunction, Request, Response } from 'express'
import { BadRequestError, NotFoundError } from '../entity/Exception'
import { verifyBook } from '../middleware/request-validator'

export default class BookController {
  private bookService: BookService = new BookService()

  findAllBooks = async (req: Request, res: Response) => {
    const books = await this.bookService.findAll()
    console.log('Books successfully retrieved')
    res.status(200).json({ books: books, message: 'Books successfully retrieved' })
  }

  findBookByTitle = async (req: Request, res: Response, next: NextFunction) => {
    const title = req.params.title
    const book = await this.bookService.findByTitle(title)
    if (!book) {
      console.error('No book found with this title')
      return next(new NotFoundError('No book found with this title'))
    }
    console.log('Book successfully retrieved')
    res.status(200).json({ book: book, message: 'Book successfully retrieved' })
  }

  addBook = async (req: Request, res: Response, next: NextFunction) => {
    const book = verifyBook(req.body)
    if (Array.isArray(book)) {
      return next(new BadRequestError(book.toString()))
    }
    const addedBook = await this.bookService.add(book)
    res.status(201).json({ book: addedBook, message: 'Book successfully created' })
  }
}
