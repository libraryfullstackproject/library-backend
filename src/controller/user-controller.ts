import { NextFunction, Request, Response } from 'express'
import { BadRequestError } from '../entity/Exception'
import { verifyUser } from '../middleware/request-validator'
import UserService from '../service/user-service'

export default class UserController {
  private userService: UserService = new UserService()

  register = async (req: Request, res: Response, next: NextFunction) => {
    const user = verifyUser(req.body)
    if (Array.isArray(user)) {
      return next(new BadRequestError(user.toString()))
    }
    const newUser = await this.userService.register(user)
    console.log('User successfully created !')
    res.status(201).json({ message: 'User successfully created ! ', user: newUser })
  }

  login = async (req: Request, res: Response) => {
    const authorization = req.headers.authorization
    const token = await this.userService.login(authorization)
    console.info('User successfully logged in !')
    res.status(200).json({ message: 'User successfully logged in !', token: token })
  }
}
