import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export default class Book {
  @PrimaryGeneratedColumn()
  public id?: number

  @Column()
  public title: string

  @Column()
  public year: number

  @Column()
  public description: string

  @Column()
  public author: string

  @Column()
  public price: number

  @Column({ default: 20 })
  public stock?: number
}
