import { Entity, JoinColumn, OneToOne, PrimaryColumn } from 'typeorm'
import Book from './Book'
import User from './User'

@Entity()
export default class Favorite {
  @PrimaryColumn()
  @OneToOne(() => User)
  @JoinColumn()
  userId: string

  @OneToOne(() => Book)
  @JoinColumn()
  @PrimaryColumn()
  bookId: number
}
