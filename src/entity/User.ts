import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export default class User {
  @PrimaryGeneratedColumn()
  public id?: number

  @Column()
  public firstName: string

  @Column()
  public lastName: string

  @Column()
  public email: string

  @Column()
  public password: string

  @Column()
  public isAdmin: boolean

  @Column({ default: 200 })
  public balance?: number
}
