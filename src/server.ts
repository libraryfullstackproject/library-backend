import app from './app'
import { SERVER_PORT } from './lib/dotenv'

//Launch server
const PORT: number = parseInt(SERVER_PORT, 10)

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`)
})
