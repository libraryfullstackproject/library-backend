import { verify, sign } from 'jsonwebtoken'
import { SECRET } from '../lib/dotenv'
import { Request, Response, NextFunction } from 'express'
import { AuthenticationFailureError } from '../entity/Exception'
import { comparePasswords } from './identification'

const extractToken = (authorization: string | undefined): string | undefined => {
  return authorization?.split(' ')[1]
}

export const getDecodedPayload = (req: Request, res: Response, next: NextFunction) => {
  const token = extractToken(req.headers.authorization)
  if (token) {
    try {
      return verify(token, SECRET)
    } catch (err) {
      console.error('Invalid or expired token')
      return next(new AuthenticationFailureError('Invalid or expired token'))
    }
  }
}

export const requireAuthentication = (req: Request, res: Response, next: NextFunction) => {
  if (verifyToken(req, res, next)) {
    return next()
  }
}

export const verifyToken = (req: Request, res: Response, next: NextFunction) => {
  const isValidToken = getDecodedPayload(req, res, next)
  return isValidToken ? true : false
}

export const getToken = async (
  id: number | undefined,
  passwordEntered: string,
  databasePassword: string,
  isAdmin: boolean
) => {
  const payload = { id: id, isAdmin: isAdmin }
  const base64password = Buffer.from(passwordEntered, 'base64').toString('utf-8')
  if (await comparePasswords(base64password, databasePassword)) {
    return sign({ user: payload }, SECRET, { expiresIn: '1h' })
  }
}
