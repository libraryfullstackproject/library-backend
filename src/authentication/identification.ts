import { hash, compare } from 'bcryptjs'

export const hashPassword = async (password: string) => {
  return await hash(Buffer.from(password, 'base64').toString('utf-8'), 10)
}

export const comparePasswords = async (base64password: string, databasePassword: string) => {
  return await compare(base64password, databasePassword)
}
