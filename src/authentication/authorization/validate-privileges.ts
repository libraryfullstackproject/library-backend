import { Request, Response, NextFunction } from 'express'
import { getDecodedPayload } from '../jwt'
import { UnauthorizedError } from '../../entity/Exception'

export const requireAdminRole = (req: Request, res: Response, next: NextFunction) => {
  const decoded = getDecodedPayload(req, res, next)
  if (decoded) {
    const decodedUser = decoded.user
    if (!decodedUser.isAdmin) {
      console.error("You don't have the sufficient rights to perform this action")
      return next(new UnauthorizedError("You don't have the sufficient rights to perform this action"))
    }
  }
}
