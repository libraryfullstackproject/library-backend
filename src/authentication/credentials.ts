export const decodeEmailAndPassword = (authorization: string) => {
  if (authorization && authorization.split(' ')[0] === 'Basic') {
    const base64Credentials = authorization.split(' ')[1]
    const credentials = Buffer.from(base64Credentials, 'base64').toString('utf-8')
    const creds = credentials.split(':')
    return creds.length === 2 ? creds : []
  }
  return []
}
