import { Request, Response, NextFunction } from 'express'
import { BadRequestError } from '../entity/Exception'
import { verifyToken } from './jwt'

export const requireAuthentication = (req: Request, res: Response, next: NextFunction) => {
  if (verifyToken(req, res, next)) {
    return next()
  }
}

export const requireCredentials = (req: Request, res: Response, next: NextFunction) => {
  const authorization = req.headers.authorization
  if (!authorization || !(authorization.split(' ')[0] === 'Basic') || authorization.split(' ')[1].length === 0) {
    console.error('Please provide basic auth credentials')
    return next(new BadRequestError('Please provide basic auth credentials'))
  }
  return next()
}
