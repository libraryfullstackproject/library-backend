import User from '../entity/User'
import { AppDataSource } from '../database/postgres-connection'
import { AuthenticationFailureError, BadRequestError, NotFoundError, ResourceConflictError } from '../entity/Exception'
import { hashPassword } from '../authentication/identification'
import { decodeEmailAndPassword } from '../authentication/credentials'
import { getToken } from '../authentication/jwt'

export default class UserService {
  private userRepository = AppDataSource.getRepository(User)

  getByEmail = async (email: string) => {
    return await this.userRepository.findOneBy({ email: email })
  }

  getByFirstNameAndLastName = async (firstName: string, lastName: string) => {
    return await this.userRepository.findOneBy({
      firstName: firstName,
      lastName: lastName
    })
  }

  register = async (user: User) => {
    const existingUser = await this.getByFirstNameAndLastName(user.firstName, user.lastName)
    if (existingUser) {
      throw new ResourceConflictError('A user with this name already exists')
    }
    user.password = await hashPassword(user.password)
    return await this.userRepository.save(user)
  }

  login = async (authorization: string | undefined) => {
    if (!authorization) {
      throw new BadRequestError('Please provide Basic auth credentials')
    }
    const credentials = decodeEmailAndPassword(authorization)
    const existingUser = await this.getByEmail(credentials[0])
    if (!existingUser) {
      throw new NotFoundError('No user found with this email address')
    }
    const token = await getToken(existingUser.id, credentials[1], existingUser.password, existingUser.isAdmin)
    if (!token) {
      throw new AuthenticationFailureError('Invalid password')
    }
    return token
  }
}
