import Book from '../entity/Book'
import { AppDataSource } from '../database/postgres-connection'
import { ResourceConflictError } from '../entity/Exception'

export default class BookService {
  private bookRepository = AppDataSource.getRepository(Book)

  findAll = async () => {
    const books = await this.bookRepository.find()
    const allBooks: Array<Book> = []
    books.forEach((book) => allBooks.push(book))
    return allBooks
  }

  findByTitle = async (title: string) => {
    return await this.bookRepository.findOneBy({ title: title.toUpperCase() })
  }

  findByTitleAndAuthor = async (title: string, author: string) => {
    return await this.bookRepository.findOneBy({ title: title.toUpperCase(), author: author })
  }

  add = async (book: Book) => {
    const existingBook = await this.findByTitleAndAuthor(book.title, book.author)
    if (existingBook) {
      throw new ResourceConflictError('A book with this title and author already exists')
    }
    book.title = book.title.toUpperCase()
    return await this.bookRepository.save(book)
  }
}
