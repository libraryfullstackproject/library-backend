import User from './entity/User'

declare module 'jsonwebtoken' {
  function verify(token: string, secretOrPublicKey: Secret, options?: VerifyOptions): DecodedJwtToken
  function sign(
    payload: string | object | Buffer,
    secretOrPrivateKey: jwt.Secret,
    option?: jwt.SignOptions | undefined
  ): string

  export interface DecodedJwtToken {
    user: User
  }
}
