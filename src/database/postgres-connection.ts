import { DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_NAME, DATABASE_PORT } from '../lib/dotenv'
import { DataSource } from 'typeorm'
import Book from '../entity/Book'
import User from '../entity/User'
import Favorite from '../entity/Favorite'

export const AppDataSource = new DataSource({
  type: 'postgres',
  host: DATABASE_HOST,
  port: Number(DATABASE_PORT),
  username: DATABASE_USER,
  password: DATABASE_PASSWORD,
  database: DATABASE_NAME,
  synchronize: true,
  logging: false,
  entities: [Book, User, Favorite],
  subscribers: [],
  migrations: []
})

AppDataSource.initialize()
  .then(() => {
    console.log('Succesfully connected to Postgres database!')
  })
  .catch((error) => console.error(error))
