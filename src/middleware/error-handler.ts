import { BaseError } from '../entity/Exception'
import { Request, Response, NextFunction } from 'express'

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const errorHandler = (err: TypeError | BaseError, req: Request, res: Response, next: NextFunction) => {
  let baseError = err
  if (!(err instanceof BaseError)) {
    baseError = new BaseError('Something wrong happend')
  }
  res.status((baseError as BaseError).status).send(baseError)
}

export default errorHandler
