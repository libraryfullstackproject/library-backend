import { Request, Response, NextFunction } from 'express'

export const autoCatch = (middleware: (req: Request, res: Response, next: NextFunction) => Promise<void>) => {
  return (req: Request, res: Response, next: NextFunction) => {
    middleware(req, res, next).catch((err) => {
      next(err)
    })
  }
}
