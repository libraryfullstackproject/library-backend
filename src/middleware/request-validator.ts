import * as t from 'io-ts'
import { PathReporter } from 'io-ts/PathReporter'
import { isRight } from 'fp-ts/lib/Either'
import * as EmailValidator from 'email-validator'

const isValidEmail = (email: string) => {
  return EmailValidator.validate(email)
}

const RuntimeBook = t.exact(
  t.intersection([
    t.partial({ id: t.number, stock: t.number }),
    t.type({
      title: t.string,
      year: t.number,
      description: t.string,
      author: t.string,
      price: t.number
    })
  ])
)

const RuntimeUser = t.exact(
  t.intersection([
    t.partial({ id: t.number, balance: t.number }),
    t.type({
      firstName: t.string,
      lastName: t.string,
      email: t.string,
      password: t.string,
      isAdmin: t.boolean
    })
  ])
)

const decodeBook = (body: unknown) => RuntimeBook.decode(body)
const decodeUser = (body: unknown) => RuntimeUser.decode(body)

const isValidBookBody = (body: unknown): body is Book => isRight(decodeBook(body))
const isValidUserBody = (body: unknown): body is User => isRight(decodeUser(body))

export const verifyBook = (body: unknown) => {
  return isValidBookBody(body) ? body : PathReporter.report(decodeBook(body))
}

export const verifyUser = (body: unknown) => {
  if (isValidUserBody(body)) {
    if (isValidEmail(body.email)) {
      return body
    }
    return ['Please provide a valid email']
  }
  return PathReporter.report(decodeUser(body))
}

export type Book = t.TypeOf<typeof RuntimeBook>
export type User = t.TypeOf<typeof RuntimeUser>
