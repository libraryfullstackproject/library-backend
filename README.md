# Library Backend

## Author
Victoria Doe

## Software
- Node JS v16.13.1 - Express v4.17.1
- Typescript - 4.7.4
- PostgreSQL  
- Docker  

## How to run the project

To run the project locally, first make sure you have Node and npm installed.  
Clone this [URL]("https://gitlab.com/libraryfullstackproject/library-backend.git") 

You must first install all the node_modules by executing the following command :  
```bash
  npm install
```

To launch the database, open a terminal at the root of the project (outside the src folder) :    
```bash
  docker-compose up
```

To run the project in development mode :  
```bash
  npm run dev
```

To run the project in production mode : (npm run start will automatically build the project)  
```bash
  npm run start
```

To run the tests :
```bash
  npm run build
  npm run test
```

To shut the database down :    
```bash
  docker-compose down
```

You will also need to set the following environment variables in a .env file at the root of the project (do not wrap variables in quotes):      

| Environment Variable | Default Value |   Type    |
| -------------------- | ------------- | --------- |
| SERVER_PORT          | 3005          | integer   |
| DATABASE_USER        | hidden        | string    |
| DATABASE_PASSWORD    | hidden        | string    |
| DATABASE_NAME        | hidden        | string    |
| DATABASE_PORT        | 5432          | integer   |
| DATABASE_HOST        | hidden        | string    |
| SECRET               | hidden        | string    |


This .env file is important for the docker-compose.yml to start the database

## Subject

A library to get all available books and purchase some of them.

## Queries
 
1. Get all books  
2. Get a book by title  
3. Create a book  
4. Create a user  
5. Log a user in  


### Todo
- Get a user's favorite books  
- Add a book to a user's favorites  
- Purchase a book  
- Delete a book from favorites
