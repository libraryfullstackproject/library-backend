module.exports = {
  semi: false,
  trailingComma: 'none',
  printWidth: 120,
  tabWidth : 2,
  singleQuote: true,
  bracketSpacing: true
};
